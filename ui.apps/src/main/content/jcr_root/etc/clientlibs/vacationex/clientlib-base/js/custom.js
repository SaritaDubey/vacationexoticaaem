$(document).ready(function () {
    // Global variable
    var windowWidth = $(window).width();
    var headerHeight = $('.ham-navigation').outerHeight(true);
    var footerHeight = $('.footer').outerHeight(true);
    var SumHeaderFooter = headerHeight + footerHeight;

    //Function for the page
    //get the height of the title and subtitle and apply the height accordingly to .package-details div
    function updateContainerHeight(t) {

        if ($('.cd-main-content').find('.custom-height').length > 0) {
            if (typeof t !== 'undefined') {
                //added 10px to make the gutter space at the bottom of the block
                var setPackageDetails = $(window).height() - $(t).find('.custom-height').offset().top - footerHeight - 10;
            } else {
                var setPackageDetails = $(window).height() - $('.custom-height').offset().top - footerHeight - 10;
            }
            $('.custom-height').css('height', setPackageDetails);

        } else {
            //Category and themed holiday page container height 
            //164 => sum of logo container & footer height 
            var gutterSpace = SumHeaderFooter + 100;
            var setPackageDetails = ($(window).height() - gutterSpace) / 2;
            $('.holiday-thumbnail').css('height', setPackageDetails);
        }
    }

    // js while apply in mobile or device less than 768px
    function convertTabIntoDropDown() {
        var c_tab;
        jQuery('.tabs-menu').addClass('toggleTabMenu');
        if ($(".package-detail-page .tabs-menu li").hasClass('current')) {
            c_tab = $('.package-detail-page .tabs-menu li.current').text();
        } else {
            c_tab = $(".package-detail-page .tabs-menu li:first-child a").text();
        }

        jQuery('body').on('click', '.tabs-menu a', function () {
            c_tab = jQuery(this).text();
            //jQuery('.tabs-menu li').removeClass('current');
            // jQuery(this).closest('li').addClass('current');
            // var c_city_icon = jQuery(this).find('img').data('id');
            jQuery('#current-tab').text(c_tab);
            jQuery('.tabs-menu').removeClass('active-tab');
            //hide = false;
        });

        jQuery('#current-tab').text(c_tab);

        jQuery('body').on('click', '#current-tab', function () {
            jQuery('.toggleTabMenu').toggleClass('active-tab');
        });
        // if( windowWidth <= 768){
        // 	jQuery('.tabs-menu').addClass('toggleTabMenu');
        // 	//Convert tab menu into drop down in mobile device

        // 	jQuery('body').on('click','#current-tab' ,function(){
        // 		console.log('in if');
        // 		jQuery('.toggleTabMenu').toggleClass('active-tab');
        // 		// if(jQuery('.toggleTabMenu').hasClass('active-tab')){
        // 		// 	jQuery('.toggleTabMenu').removeClass('active-tab'); 
        // 		// 	//hide = false;
        // 		// 	console.log('asd');
        // 		// }else{
        // 		// 	jQuery('.toggleTabMenu').addClass('active-tab');
        // 		// 	console.log('in asd'); 
        // 		// }
        // 	});
        // }
        // else{
        // 	jQuery('.tabs-menu').removeClass('toggleTabMenu');
        // }
    }
    $(document).ready(function () {
        // Initialise the function at once 

        // Update the height of the copy container and assign the height to the block
        updateContainerHeight();

        // Convert the package detail page tabs menu into drop down menu in the mobile device
        convertTabIntoDropDown();

        $(window).on('resize', function (event) {
            updateContainerHeight();
            //convertTabIntoDropDown();
            console.log('resize');

        });
        /*-------
        Category detail page 
        --------*/
        // Tab
        $(".tabs-menu a").click(function (event) {
            var tab = $(this).attr("href");
            var btnType = $(this).closest('li').data('btn-type');
            if (btnType != "anchor") {
                event.preventDefault();
                $(this).parent().addClass("current");
                $(this).parent().siblings().removeClass("current");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn();

                if ($(tab).find('.custom-height').length !== 0) {
                    updateContainerHeight(tab);
                }
            }
        });

        // Initialised slick slider - Itenerary section
        // $('.itinerary-day-summary').slick({
        // 	infinite: false,
        // 	//slidesToShow: 3,
        // 	dots: false,
        // 	slidesPerRow: 3,
        // 	rows: 2,
        // 	responsive: [
        // 	{
        // 	  breakpoint: 1400,
        // 	  settings: {
        // 		slidesPerRow: 2
        // 	  }
        // 	},
        // 	{
        // 		breakpoint: 600,
        // 		settings: "unslick"
        // 	}
        //   ]
        // });
        $('.tabs-menu a[href="#itinerary"]').on('click', function () {
            $('.itinerary-day-summary').not('.slick-initialized').slick({
                infinite: false,
                //slidesToShow: 3,
                dots: false,
                slidesPerRow: 3,
                rows: 2,
                responsive: [{
                    breakpoint: 1400,
                    settings: {
                        slidesPerRow: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: "unslick"
                }
                ]
            });
        });

        $('.landing-page-form .itinerary-day-summary').slick({
            infinite: false,
            //slidesToShow: 3,
            dots: false,
            slidesPerRow: 3,
            rows: 2,
            responsive: [{
                breakpoint: 1400,
                settings: {
                    slidesPerRow: 2
                }
            },
            {
                breakpoint: 600,
                settings: "unslick"
            }
            ]
        });
        // $('.tabs-menu a[href="#itinerary"]').on('click', function(){
        // 	$('.itinerary-day-summary').not('.slick-initialized').slick({
        // 		infinite: false,
        // 		//slidesToShow: 3,
        // 		dots: false,
        // 		slidesPerRow: 3,
        // 		rows: 2,
        // 		responsive: [
        // 		{
        // 		  breakpoint: 1400,
        // 		  settings: {
        // 			slidesPerRow: 2
        // 		  }
        // 		},
        // 		{
        // 			breakpoint: 600,
        // 			settings: "unslick"
        // 		}
        // 	  ]
        // 	});
        // });
        // Initialised slick slider - Itenerary section
        // $('.tabs-menu a[href="#itinerary"]').on('click', function(){
        // 	$('.itinerary-day-summary').slick('reinit');
        // });

        // Initialise the first tab using js as well 
        $(".package-detail-page .tabs-menu li:first-child a").trigger('click');

        //Show readmore data on click 
        if ($(window).width() >= 600) {
            jQuery('body').on('click', '.itinerary-day-summary .read-more', function (e) {
                e.preventDefault();
                var popup_content = jQuery(this).closest('.daywise-itinerary').find('.read-more-inner-block').html();
                var el = $('#itinerary .read-more-popup-block');
                $('#itinerary .read-more-popup-body').addClass('active');
                $(el).html(popup_content);
                el.mCustomScrollbar('destroy').mCustomScrollbar();

            });

            //#itinerary .read-more-popup-block:before
            jQuery('#itinerary .read-more-popup-body .close-btn').on('click', function (e) {
                $(this).closest('.read-more-popup-body').removeClass('active');
                $(this).siblings('.read-more-popup-block').mCustomScrollbar('destroy').removeClass('mCustomScrollbar').html('');
            });

            // On before slide change
            $('.itinerary-day-summary ').on('beforeChange', function () {
                $('.read-more-popup-body').removeClass('active');
                $('.read-more-popup-block').mCustomScrollbar('destroy').removeClass('mCustomScrollbar').html('');
            });
        } else {
            jQuery('body').on('click', '.itinerary-day-summary .read-more', function (e) {
                e.preventDefault();
                //var popup_content = jQuery(this).closest('.daywise-itinerary').find('.read-more-popup-content').html();
                var el = jQuery(this).closest('.daywise-itinerary').find('.read-more-inner-block');
                el.closest('.read-more-popup-content').addClass('active');
                el.addClass('mCustomScrollbar').mCustomScrollbar();
            });
            //#itinerary .read-more-popup-block:before
            //console.log('asd');
            jQuery('body').on('click', '#itinerary .read-more-popup-content .close-btn', function (e) {
                console.log('asd');
                $(this).closest('.read-more-popup-content').removeClass('active');
                $(this).siblings('.read-more-inner-block').mCustomScrollbar('destroy').removeClass('mCustomScrollbar');
                //$('body').removeClass('body-active');
            });


        }

        //Enquiry form - show errors in the popup
        $('#enquiry #submit-btn').on('click', function () {
            swal({
                title: 'Error!',
                text: 'Do you want to continue',
                type: 'error',
                confirmButtonText: 'Cool'
            })
        });

        // Show OTP field when user click on send otp button
        $('.verification-btn .v-otp-btn').on('click', function () {
            $('.hidden-btn').addClass('active');
            console.log('clicked')
            $(this).addClass('active').text('Resend OTP');
        });

        //Enquiry Form - dropdown (Select2)
        //$('.custom-drop-down').select2();

        // Open enquiry form in popup on users click
        // Open content in popup on click of read more button 
        $('.form-popup, .readmore-popup, .img_popup').magnificPopup({
            type: 'inline',
            disableOn: 0,
            //delegate: 'a',
            removalDelay: 500, //delay removal by X to allow out-animation
            callbacks: {
                beforeOpen: function () {
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            },
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });
        $('.video_popup').magnificPopup({
            disableOn: 0,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });

        // FAQS Accordion
        $(".faqs-accordion .package-place-name").on('click', function (e) {
            e.preventDefault();
            var current_accord_block = $(this).closest('.faqs-accordion');
            current_accord_block.find(".package-place-name").removeClass('active');
            $(this).addClass('active');
            var openPanel = $(this).siblings(".package-place-highlights");
            openPanel.slideDown();
            current_accord_block.find(".package-place-highlights").not(openPanel).slideUp();
        });

        // TNC Accordion
        $(".faqs-accordion .accord-title").on('click', function (e) {
            console.log('asd');
            e.preventDefault();
            $(".faqs-accordion .accord-title").removeClass('active');
            $(this).addClass('active');
            var openPanel = $(this).siblings(".pointer");
            openPanel.slideDown();
            $(".faqs-accordion").find(".pointer").not(openPanel).slideUp();
        });

        /*-------
        Package Listing page
        --------*/
        //match the height of the grid of the same row
        $('.mobileMatchHeight').matchHeight();

        //Load more content
        $(function () {
            $(".packages-list ul li").slice(0, 4).show();
            $(".load-more").on('click', function (e) {
                e.preventDefault();
                $(this).removeClass('bounce');
                $(".packages-list ul li:hidden").slice(0, 4).slideDown();
                if ($(".packages-list .custom-height").length == 0 && $(window).width() > 768) {
                    $('.packages-list ul').addClass('mCustomScrollbar custom-height').mCustomScrollbar();
                    updateContainerHeight();
                }
                $('.packages-list ul li').animate({
                    scrollTop: $('body').offset().top
                }, 1500);
            });
        });

        /********* 
        Categories Drop down
        *********/
        //Show the dropdown values on click of catetogry block
        $("body").on('click', '.selecting-option', function (e) {
            e.preventDefault();
            $('body').addClass('asd');
            $('.package-multi-option').toggleClass('dropdown-open')
            $(this).toggleClass('active')
        });

        $("body").click(function (e) {
            if ($('.selecting-option.active').length == 1) {
                console.log(e.target)
                if (!$(e.target).hasClass('selecting-option') && !$(e.target).closest('div').hasClass('package-multi-option')) {
                    console.log(e.target.className);
                    console.log('clicked on body');
                    $('.package-multi-option').removeClass('dropdown-open')
                    $('.selecting-option').removeClass('active');
                }
            }
        });

        /* $("body").not('.selecting-option').on('click', function (e) {
            e.preventDefault();
            console.log('clicked on body');
            $('.package-multi-option').removeClass('dropdown-open')
            $('.selecting-option').removeClass('active');
        }); */

        /* On selection of values from dropdown, 
         * append the value in 'selected value' div and 
         * add Check sign to the selected value of drop down only
         * Close the dropdown when user click on the Category div 
         */
        $("body").on('click', '.package-multi-option li', function (e) {
            e.preventDefault();
            var $this = $(this);
            if (!$this.hasClass('selected-value')) {
                var selectedValue = $this.html();
                $('.package-selected-option').append(selectedValue);
                $this.addClass('selected-value');
            } else {
                $this.removeClass('selected-value');
            }
        });
        /* on click of close button (span) check the closes data-option attr
         * and store into var storeDataOption
         * then run each loop for all the li of dropdown 
         * and compare the value of storeDataOption and data-optionn value of
         * dropdown and remove the check icon from it
         */
        $("body").on('click', '.package-selected-option a span', function (e) {
            e.preventDefault();
            var $this = $(this);
            var storeDataOption = $this.closest('a').attr('data-option');
            $('.package-multi-option').find('li').each(function (index) {
                if ($(this).find('a').attr('data-option') == storeDataOption) {
                    $(this).closest('li').removeClass('selected-value');
                }
            });
            $this.closest('a').remove();
        });

        // $('.content-slider-block').slick({
        // 	dots: false,
        // 	infinite: false,
        // 	speed: 300,
        // 	slidesToShow: 1,
        // 	//variableWidth: true
        // });

        // // Initialised slick slider - Itenerary section
        // $('.v-two-block .tabs-menu a').on('click', function(){
        // 	$('.content-slider-block').slick('reinit');
        // });

        if (windowWidth <= 768) {
            // Home page
            // dragdealer in mobile device
            var dragger_slider = new Dragdealer('dragger-slider', {
                x: 0,
                // Start in the bottom-left corner
                y: 1,
                vertical: true,
                speed: 0.2,
                loose: true
            });
        }

        //   landing page - package detail page template
        // insitailaize slick slider
        //$('.tabs-menu a[href="#itinerary"]').on('click', function(){
        $('.landing-p  .itinerary-day-summary').slick({
            infinite: false,
            //slidesToShow: 3,
            dots: false,
            slidesPerRow: 3,
            rows: 2,
            responsive: [{
                breakpoint: 1400,
                settings: {
                    slidesPerRow: 2
                }
            },
            {
                breakpoint: 600,
                settings: "unslick"
            }
            ]
        });
        //});
        $('#map-img[usemap]').rwdImageMaps();

        if ($('body').find('.package-detail-page').length > 0) {
            // console.log($(window).height());
            var setPackageDetails = $(window).height() - SumHeaderFooter;
            console.log(setPackageDetails);
            var newHeight = SumHeaderFooter + $('.package-detail-tab').offset().top
            var setPackageDetails = $(window).height() - newHeight;

            console.log(setPackageDetails);
            $('.with-scroll-page').find('.tab-content').css('min-height', setPackageDetails);
            console.log($('.package-detail-tab').offset().top);
        }
        /*$(window).on('resize', function(event){
            if($('body').find('.package-detail-page').length > 0){
                var setPackageDetails = $(window).height() - SumHeaderFooter;
                console.log(setPackageDetails);
                var newHeight = SumHeaderFooter + $('.package-detail-tab').offset().top
                var setPackageDetails = $(window).height() - newHeight ;
                // var setPackageDetails = $(window).height() - $('.package-detail-tab').offset().top;
                $('.with-scroll-page').find('.tab-content').css('min-height', setPackageDetails);
            }	
            // console.log( $(window).height());
            console.log($('.package-detail-tab').offset().top);
            // console.log(setPackageDetails);
        });*/

        // testimonial Page
        $('.testimonial-slider').slick({
            infinite: false,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });

        $('.slider-thumbnail').slick({
            infinite: false,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false
        });
    });
});