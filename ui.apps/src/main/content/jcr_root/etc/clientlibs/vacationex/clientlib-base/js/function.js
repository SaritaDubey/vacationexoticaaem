
//$("#submit-btn").prop('disabled', 'disabled');
	// Price on Request From js
	$(".otp").hide();

	$.magnificPopup.instance._onFocusIn = function(e) {
		// Do nothing if target element is select2 input
		if( $(e.target).hasClass('select2-search__field') ) {
			return true;
		} 
		// Else call parent method
		$.magnificPopup.proto._onFocusIn.call(this,e);
	};

	$("#package").select2({
		ajax: {
			url: site_base_url + 'packages_detail/get_all_packages',
			dataType: 'json',
			data: function (params) {
				var query = {
					q: params.term,// search term
				}
				return query;
			},
			processResults: function (data) {
				return {
					results: data
				}
			}
		}
	});
	
	var preventclick = false;
	var enquiry_record_id = '';
	$('.enquiry_send_otp_button').click(function(e){
		e.preventDefault();
		var btn = $(this);
		var frm = $(btn).closest('form');
		var mobile = jQuery.trim($(frm).find("#mobile").val());
		var mobile_regex=/^\d{10}$/;
		if(mobile == ""){
			swal(
				'Oops!',
				'Please enter mobile.',
				'error'
			)
			return false;
		}
		if(mobile!=""){
			if(!mobile_regex.test(mobile)){debugger ;
				swal(
					'Oops!',
					'Please enter 10 digit mobile number.',
					'error'
				)
				return false;
			}
		}
		var desination_present = $(frm).find('#destination').val().length;
		if(desination_present > 0){
			var destination = $(frm).find('#destination').val();	
		}
		else{
			var destination = $("input[name=hidden-destination]").val();
		}
		var form_data = {           
			name: $(frm).find('#name').val(),
			email: $(frm).find('#email').val(),
			mobile: $(frm).find('#mobile').val(),
			city: $(frm).find('#city').val(),
			destination: destination,
			add_desc: $(frm).find('#add_desc').val(),
			tnc: $(frm).find('#tnc:checked').val(),
			current_package_id: $(frm).find('#current-package-id').val(),
			record_id: enquiry_record_id, 
			utm_source: utm_source, 
			utm_medium: utm_medium, 
			utm_campaign: utm_campaign, 
			utm_term: utm_term, 
			utm_content: utm_content 
		};
		$.ajax({
			method: "POST",
			url: site_base_url + "forms/send_otp_enquiry/"+mobile,
			data: form_data,
            beforeSend: function(){
				$(btn).text('Sending...');
                $(frm).find('#mobile').prop('disabled', 'disabled');
            },
			success: function(res) {
				$(btn).text('Send OTP');
                $(frm).find('#mobile').prop('disabled', false);
				var j_res = JSON.parse(res);
				enquiry_record_id = j_res.data;
				if(j_res.status =="success"){
					swal(
						'Success!',
						'OTP has been sent on your number',
						'success'
					)
					$(frm).find('#mobile').prop('disabled', 'disabled');
					$(frm).find('.hidden-btn').addClass('active');
					$(btn).addClass('disabled').css('pointer-events', 'none');
				} else {
					swal(
						'Oops!',
						'Error while sending OTP',
						'error'
					)
				}
			},
			error: function(){
				$(btn).text('Send OTP');
                $(frm).find('#mobile').prop('disabled', false);
			}
		});
	});

	$(".verify_otp_button").click(function(e){
		e.preventDefault();
		var frm_wrapper = $(this).closest('form');
		var verify_otp = $(frm_wrapper).find("#verify_otp").val();
		if(verify_otp == ""){
			swal(
				'Oops!',
				'Please enter OTP.',
				'error'
			)
			return false;
		}
		$.ajax({
			method: "POST",
			url: site_base_url + "forms/verify_otp/"+verify_otp,
            beforeSend: function(){
				$(frm_wrapper).find('.verify_otp_button').text('Checking...');
				$(frm_wrapper).find("#verify_otp").prop('disabled', 'disabled');
            },	
			success: function(res) {
				$(frm_wrapper).find('.verify_otp_button').text('Verify OTP');
				var j_res = JSON.parse(res);
				if(j_res.status =="valid"){
					swal(
						'Success!',
						'OTP successfully verified.',
						'success'
					);
					$(frm_wrapper).find('#verify_otp').prop("disabled", 'disabled');
					$(frm_wrapper).find('.verify_otp_button').addClass('disabled').css('pointer-events', 'none');
					$(frm_wrapper).find('#submit-btn').prop("disabled", false);
					$(frm_wrapper).find('#otp_status').val('true');
				}else{
					swal(
						'Oops!',
						'Invalid OTP',
						'error'
					)
					$(frm_wrapper).find('#verify_otp').prop("disabled", false);
					$(frm_wrapper).find("#submit-btn").prop('disabled', 'disabled');
				}
			},
			error: function(){
				$(frm_wrapper).find('.verify_otp_button').text('Verify OTP');
				$(frm_wrapper).find('#verify_otp').prop("disabled", false);
				$(frm_wrapper).find("#submit-btn").prop('disabled', 'disabled');
			}
		});
	});

	//  Destination autocomplete tags for #enquiry_form
	// var tagApi_enquiry = $(".enquiry-tm-input").tagsManager();

	// var sf_menu_sub_enquiry = $('.enquiry-suggesstion-box');
	// $(document).on('click', function (e) {
	// 	sf_menu_sub_enquiry.hide();
	// });

	// $(document).on('click', '#enquiry-form .destination-selected', function (e) {
	// 	e.stopPropagation();
	// 	sf_menu_sub_enquiry.hide();
	// 	tagApi_enquiry.tagsManager("pushTag", $(this).html());
	// });

	// $(".enquiry-destination").attr('autocomplete', 'off').keyup(function(e){
	// 	$.ajax({
	// 		url: site_base_url + 'packages_detail/get_all_packages_destination/' + $(this).val(),
	// 		method: 'post',
	// 		beforeSend: function(){
	// 		},
	// 		success: function(data){
	// 			sf_menu_sub_enquiry.html(data).show();
	// 		}
	// 	});	
	// });
	$('#enquiry-form').on('submit', function(e){
		var j_res = '';
		e.preventDefault();
		var frm = $(this);
		var desination_present = $(frm).find('#destination').val().length;
		if(desination_present > 0){
			var destination = $(frm).find('#destination').val();	
		}
		else{
			var destination = $("input[name=hidden-destination]").val();
		}
		var form_data = {           
			name: $(frm).find('#name').val(),
			email: $(frm).find('#email').val(),
			mobile: $(frm).find('#mobile').val(),
			verify_otp: $(frm).find('#verify_otp').val(),
			otp_status: $(frm).find('#otp_status').val(),
			city: $(frm).find('#city').val(),
			destination: destination,
			add_desc: $(frm).find('#add_desc').val(),
			tnc: $(frm).find('#tnc:checked').val(),
			current_package_id: $(frm).find('#current-package-id').val(),
			record_id: enquiry_record_id, 
			utm_source: utm_source, 
			utm_medium: utm_medium, 
			utm_campaign: utm_campaign, 
			utm_term: utm_term, 
			utm_content: utm_content 
		};
		$.ajax({
			method: "POST",
			url: site_base_url + "forms/save_enquiry",
			data: form_data,
            beforeSend: function(){
				$(frm).find('#submit-btn').val('Loading...');
                $(frm).find('submit, input,select').prop('disabled', 'disabled');
            },
			success: function(res) {
				$(frm).find('#submit-btn').val('GET IN TOUCH');
			    $(frm).find('submit,input,select').prop('disabled', '');
				$('.enquiry_send_otp_button').removeClass('disabled').css('pointer-events', 'auto');
				var j_res = JSON.parse(res);
				try {
					if(j_res.status == 'success') {
						enquiry_record_id = '';
						var lead_id = j_res.lead_id;
						$('#otp_status').val('');
						$('.hidden-btn').removeClass('active');
						$('.verification-btn .v-otp-btn').removeClass('active').text('Send OTP');
						swal(
							'Success!',
							'Thank you for your interest in this spectacular tour by Vacations Exotica. This is the first step on your journey to collecting incredible stories from around the world. For the next step, one of our representatives will get in touch with you, soon.',
							'success'
						);
						dataLayer.push('send', {
							'event': 'form-submission',
							'lead_id' : 'enquiry'+lead_id,
							'formType': 'Enquiry',
							'formPosition': window.location.href
						});
						$( '#enquiry-form' ).each(function(){
							this.reset();
						});
						// tagApi_enquiry.tagsManager('empty');
						$(frm).find('.enquiry_send_otp_button, .verify_otp_button').removeClass('disabled').css('pointer-events', 'auto');
					} 
					else{
						swal(
							'Oops!',
							j_res.msg,
							'error'
						)
					}
				}
				catch (e) {
					swal(
						'Oops!',
						'Something went wrong!',
						'error'
					)
				}	
			},
			error: function(){
				$(frm).find('#submit-btn').val('GET IN TOUCH');
                $(frm).find('submit,input,select').prop('disabled', '');
				swal(
					'Oops!',
					'Something went wrong!',
					'error'
				)
			}
		});
	});

