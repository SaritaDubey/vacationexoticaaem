package apps.vacationex.components.content;

import java.util.List;

import com.adobe.cq.sightly.WCMUsePojo;
import com.mirum.vacationex.core.dao.PackageListingPage;
import com.mirum.vacationex.core.models.PackageListingModel;

public class PackageListingHelper extends WCMUsePojo {

	List<PackageListingModel> model;

	@Override
	public void activate() throws Exception {
		PackageListingPage packageListingPage = getSlingScriptHelper().getService(PackageListingPage.class);
		this.model = packageListingPage.populateData();
	}

	public List<PackageListingModel> getPackageList(){
		return model;
		
	}
}
