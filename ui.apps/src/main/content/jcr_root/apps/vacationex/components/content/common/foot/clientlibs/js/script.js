$(document).ready(function(){
	var record_id = '';
	$('#corporate_form').on('submit', function(e){
		e.preventDefault();
        var frm             = $(this);
        var company_name    = $(frm).find('#company_name').val();
        var name            = $(frm).find('#name').val();
        var mobile          = $(frm).find('#mobile').val();
        var email           = $(frm).find('#email').val();
		var city            = $(frm).find('#city').val();
		var otp_status      = $(frm).find('#otp_status').val();
		var occasion      	= $('input[name=occasion]:checked').val();

        $.ajax({
            url: 'http://www.vacationsexotica.com/corporate/save',
            method: 'post',
            data: {
                company_name: company_name,
				name: name,  
                mobile: mobile,
                email: email,
                city: city, 
				otp_status: otp_status,
                occasion: occasion, 
				record_id: record_id, 
                utm_source: utm_source, 
                utm_medium: utm_medium, 
                utm_campaign: utm_campaign, 
                utm_term: utm_term, 
                utm_content: utm_content 
            },
            beforeSend: function(){
				$(frm).find('#submit-btn').val('Loading...');
                $(frm).find('submit, input,select').prop('disabled', 'disabled');
            },
            success: function(res){
				$(frm).find('#submit-btn').val('SUBMIT');
                $(frm).find('submit,input,select').prop('disabled', '');
                try {
                var j_res = JSON.parse(res);
                if(j_res.status == 'success') {
					record_id = '';
					var lead_id = j_res.lead_id;
					document.getElementById("corporate_form").reset();
					$('#otp_status').val('');
					$('.hidden-btn').removeClass('active');
					$('.verification-btn .v-otp-btn').removeClass('active').text('Send OTP');
                    swal(
                        'Success!',
                        'Thank you for your interest in this spectacular tour by Vacations Exotica. This is the first step on your journey to collecting incredible stories from around the world. For the next step, one of our representatives will get in touch with you, soon.',
                        'success'
                    );
					dataLayer.push('send', {
						'event': 'form-submission',
						'lead_id' : 'corperate'+lead_id,
						'formType': 'Corporate',
						'formPosition': window.location.href
					});
					$(frm).find('.corporate_send_otp_button, .verify_otp_button').removeClass('disabled').css('pointer-events', 'auto');
                } else {
                    swal('Oops!',j_res.msg)
                }
                } catch (e) {
					swal('Oops!','Something went wrong!')
                }
            },
            error: function(){
				$(frm).find('#submit-btn').val('SUBMIT');
                $(frm).find('button, input,select').prop('disabled', '');
            }
        });
    });

	$('.corporate_send_otp_button').click(function(e){
		e.preventDefault();
		var btn = $(this);
		var frm_wrapper = $(btn).closest('form');
		var mobile = jQuery.trim($(frm_wrapper).find("#mobile").val());
		var mobile_regex=/^\d{10}$/;
		if(mobile == ""){
			swal('Oops!','Please enter mobile.','error')
			return false;
		}
		if(mobile!=""){
			if(!mobile_regex.test(mobile)){
				swal('Oops!','Please enter 10 digit mobile number.','error')
				return false;
			}
		}
		var frm = $(this).closest('form');
		
		var form_data = {       
			company_name: $(frm).find('#company_name').val(),
			name: $(frm).find('#name').val(),
			email: $(frm).find('#email').val(),
			mobile: $(frm).find('#mobile').val(),
			city: $(frm).find('#city').val(),
			occasion: $(frm).find('input[name=occasion]:checked').val(),
			record_id: record_id, 
			utm_source: utm_source, 
			utm_medium: utm_medium, 
			utm_campaign: utm_campaign, 
			utm_term: utm_term, 
			utm_content: utm_content 
		};

		$.ajax({
			method: "POST",
			url: site_base_url + "forms/send_otp_corporate/"+mobile,
			data: form_data,
            beforeSend: function(){
				$(btn).text('Sending...');
                $(frm_wrapper).find('#mobile').prop('disabled', 'disabled');
            },
			success: function(res) {
				$(btn).text('Send OTP');
                $(frm_wrapper).find('#mobile').prop('disabled', false);
				var j_res = JSON.parse(res);
				record_id = j_res.data;
				if(j_res.status =="success"){
					swal('Success!','OTP has been sent on your number','success')
					$(frm_wrapper).find('#mobile').prop('disabled', 'disabled');
					$(frm_wrapper).find('.hidden-btn').addClass('active');
					$(btn).addClass('disabled').css('pointer-events', 'none');
				} else {
					swal('Oops!','Error while sending OTP','error')
				}
			},
			error: function(){
				$(btn).text('Send OTP');
                $(frm_wrapper).find('#mobile').prop('disabled', false);
			}
		});
	});
});