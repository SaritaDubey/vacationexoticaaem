	var contact_map, marker, bounds, infowindow, active_marker_ele, center, myLatLng, my_latitude = 0, my_longitude = 0, scrolled = false;
	function initMap(){
		infowindow = new google.maps.InfoWindow();
		bounds = new google.maps.LatLngBounds();
		var myLatLng = {lat: 19.0760, lng: 72.8777};
		
        contact_map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: myLatLng,
			scrollwheel: true,
			mapTypeId: 'terrain'
        });
		$('#our-offices-list li a').not('.getintouvh-btn a').on('click', function(e){
			e.preventDefault();
			active_marker_ele = $(this);
			if(marker && typeof marker !== 'undefined') {
				marker.setMap(null);
			}
			$('#our-offices-list li').removeClass('active');
			$(this).closest('li').addClass('active');

			if($(window).width() <= 768 && scrolled) {
				$('html, body').animate({
					scrollTop: $("#all-offices-wrapper").offset().top - 10
				}, 1000);
			}
			scrolled = true;
			if($(active_marker_ele).data('latitude') && $(active_marker_ele).data('latitude') > 0) {
				center = new google.maps.LatLng($(active_marker_ele).data('latitude'), $(active_marker_ele).data('longitude'));
				marker = new google.maps.Marker({
					position: center,
					map: contact_map,
					animation: google.maps.Animation.DROP,
					title: $(active_marker_ele).data('title')
				});
				contact_map.setCenter(center);
				
				if(my_latitude > 0) {
					var dir_url = "https://www.google.co.in/maps/dir/'" + my_latitude + "," + my_longitude + "'/" + $(active_marker_ele).data('address') + "/";
				} else {
					var dir_url = "https://www.google.co.in/maps/dir//" + $(active_marker_ele).data('address') + "/";
				}
				// var dir_url = "https://www.google.co.in/maps/dir/'" + my_latitude + "," + my_longitude + "'/'" + $(active_marker_ele).data('latitude') + "," + $(active_marker_ele).data('longitude') + "'/";
				// var dir_url = "https://www.google.co.in/maps/dir/'" +  $(active_marker_ele).data('latitude') + "," + $(active_marker_ele).data('longitude') + "'/";
				var contact_div = '';
				if($(active_marker_ele).data('contact') != '') {
					contact_div = '<br />Tel:' + $(active_marker_ele).data('contact');
				}
				infowindow.setContent(`
										<div class="iw-title">
											<b>` + $(active_marker_ele).data('title') + `</b>
										</div>
										<div class="iw-address">` + $(active_marker_ele).data('address') + contact_div + `</div>
										<a href="` + dir_url + `" target="_blank" class="iw-get-directions">Get Directions</a>
									`);
				setTimeout(function(){ infowindow.open(contact_map, marker); }, 1500)

				marker.addListener('click', function() {
					infowindow.open(contact_map, marker);
				});			
				google.maps.event.addListener(infowindow, 'closeclick',function(){
					marker = null;
				});
			}
		});
		$('#our-offices-list li:first-child a').trigger('click')
	}
	$(document).ready(function(){

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position){
				my_latitude = position.coords.latitude;
				my_longitude = position.coords.longitude;
			}, function(){console.error('Geo location - permission denied!');});
		} else {
			console.error('Geo location not supported!');
		}
		var contact_record_id = '';
		$('#getintouch-form').on('submit', function(e){
			var j_res = '';
			e.preventDefault();
			var frm = $(this);
			var form_data = {           
				name: $(frm).find('#name').val(),
				email: $(frm).find('#email').val(),
				mobile: $(frm).find('#mobile').val(),
				city: $(frm).find('#city').val(),
				//record_id: contact_record_id, 
//				utm_source: utm_source, 
//				utm_medium: utm_medium, 
//				utm_campaign: utm_campaign, 
//				utm_term: utm_term, 
//				utm_content: utm_content 
			};
			$.ajax({
				method: "GET",
				url:"/vacationex/getintouch",
				data: form_data,
				beforeSend: function(){
					$(frm).find('#submit-btn').val('Loading...');
					$(frm).find('submit, input,select').prop('disabled', 'disabled');
				},
				success: function(res) {
					$(frm).find('#submit-btn').val('Submit');
					$(frm).find('submit,input,select').prop('disabled', '');
				//	var j_res = JSON.parse(res);
					
					
					try {
						if(res) {
							//contact_record_id = '';
							//var lead_id = j_res.lead_id;
							var lead_id = "Test";
							$('#otp_status').val('');
							$('.hidden-btn').removeClass('active');
							$('.verification-btn .v-otp-btn').removeClass('active').text('Send OTP');
							swal(
								'Success!',
								'Thank you for your interest in this spectacular tour by Vacations Exotica.',
								'success'
							);
//							dataLayer.push('send', {
//								'event': 'form-submission',
//								'lead_id' :'contact'+lead_id,
//								'formType': 'contact',
//								'formPosition': window.location.href
//							});
							$( '.contact-form' ).each(function(){
								this.reset();
							});
//							$(frm).find('.contact_send_otp_button, .verify_otp_button').removeClass('disabled').css('pointer-events', 'auto');
						} 
						else{
							swal(
								'Oops!',
								j_res.msg,
								'error'
							)
						}
					}
					catch (e) {
						swal(
							'Oops!',
							'Something went wrong!',
							'error'
						)
					}	
				},
				error: function(){
					$(frm).find('#submit-btn').val('Submit');
					$(frm).find('submit,input,select').prop('disabled', '');
					swal(
						'Oops!',
						'Something went wrong!',
						'error'
					)
				}
			});
		});
	});