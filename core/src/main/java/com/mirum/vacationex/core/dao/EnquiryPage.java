
package com.mirum.vacationex.core.dao;

import java.sql.SQLException;

import com.mirum.vacationex.core.models.EnquiryModal;

public interface EnquiryPage {
	public boolean addEnquiryInfo(EnquiryModal modal) throws SQLException;
}
