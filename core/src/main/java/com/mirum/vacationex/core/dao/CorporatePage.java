package com.mirum.vacationex.core.dao;

import java.sql.SQLException;

import com.mirum.vacationex.core.models.CorporateModal;

public interface CorporatePage {
	
	public boolean addCorporateInfo(CorporateModal modal) throws SQLException;
}
