package com.mirum.vacationex.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.ContactUs;
import com.mirum.vacationex.core.dao.EmailNotification;
import com.mirum.vacationex.core.models.ContactModal;

@Component(
service = Servlet.class ,
property = { 
    "sling.servlet.paths=" + "/vacationex/getintouch",
    "sling.servlet.methods=" + HttpConstants.METHOD_GET
  }
)
public class GetinTouchForm extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String name;
	private String mobile;
	private String email;
	private String city;
	
	@Reference
	private ContactUs contactUs;
	
	@Reference
	private EmailNotification emailNotification;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		name = request.getParameter("name");
		mobile = request.getParameter("mobile");
		email = request.getParameter("email");
		city = request.getParameter("city");
		
		ContactModal contactModal = new ContactModal();
		contactModal.setName(name);
		contactModal.setMobile(mobile);
		contactModal.setEmail(email);
		contactModal.setCity(city);
		
		response.getWriter().println(contactUs.addContactInfo(contactModal));
		emailNotification.sendEmail(name, email,mobile,city);
		response.setContentType("text/html");
	}

}
