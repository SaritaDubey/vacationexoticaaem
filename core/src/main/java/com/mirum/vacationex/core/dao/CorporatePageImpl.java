package com.mirum.vacationex.core.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;
import com.mirum.vacationex.core.models.CorporateModal;
import com.mirum.vacationex.core.service.ConDbServiceImpl;


@Component(service = CorporatePage.class, property = { "service.description=Adding data of Corporate page" })
public class CorporatePageImpl implements CorporatePage {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private DataSourcePool dataSourcePool;
	private Connection con;
	private DataSource dataSource;
	
	@Override
	public boolean addCorporateInfo(CorporateModal corporateModal) {
		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "insert into tbl_ve_corporate(company_name,name,mobile,email,city,occasion) values(?,?,?,?,?,?);";

				PreparedStatement statement = con.prepareStatement(query);
				statement.setString(1, corporateModal.getCompanyName());
				statement.setString(2, corporateModal.getName());
				statement.setString(3, corporateModal.getMobile());
				statement.setString(4, corporateModal.getEmail());
				statement.setString(5, corporateModal.getCity());
				statement.setString(6, corporateModal.getOccaion());
				boolean insert=statement.executeUpdate()>0;
				return insert;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;

	}
}
