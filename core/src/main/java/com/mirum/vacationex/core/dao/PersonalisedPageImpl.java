package com.mirum.vacationex.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;
import com.mirum.vacationex.core.models.PersonalisedModal;

@Component(service = PersonalisedPage.class, property = { "service.description=Test DB Service" })
public class PersonalisedPageImpl implements PersonalisedPage {

	Logger logger = LoggerFactory.getLogger(getClass());
	@Reference
	private DataSourcePool dataSourcePool;
	private Connection con;
	private DataSource dataSource;

	@Override
	public boolean addPersonalisedInfo(PersonalisedModal modal) throws SQLException {
		StringBuffer buffer = new StringBuffer();
		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "insert into tbl_ve_personalised(name,email_id,mobile,otp_verified,city,destination,requirements,tnc) values(?,?,?,?,?,?,?,?);";

				PreparedStatement statement = con.prepareStatement(query);
				statement.setString(1, modal.getName());
				statement.setString(2, modal.getEmailId());
				statement.setString(3, modal.getMobile());
				statement.setInt(4, modal.getOtpVerified());
				statement.setString(5, modal.getCity());
				statement.setString(6, modal.getDestination());
				statement.setString(7, modal.getRequirements());
				statement.setString(8, modal.getTnc());

				boolean insert = statement.executeUpdate() > 0;
				return insert;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			buffer.append("DB Exception:").append(e).append("<br/>");
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

}
