package com.mirum.vacationex.core.models;

import java.sql.Date;

/**
 * @author onkar
 *
 */
public class PackageListingModel {

	private int id;
	private String categoryId;
	private String title;
	private String slug;
	private String subTitle;
	private String description;
	private String highlight;
	private String departureTime;
	private String backgroundImageUrl;
	private String thumbnailImageUrl;
	private String pdf;
	private int isActive;
	private int orderNum;
	private String metaTitle;
	private String metaDescription;
	private String metaKeyword;
	private int createdBy;
	private int modifiedBy;
	private Date dateCreated;
	private Date dateModified;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHighlight() {
		return highlight;
	}

	public void setHighlight(String highlight) {
		this.highlight = highlight;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getBackgroundImageUrl() {
		return backgroundImageUrl;
	}

	public void setBackgroundImageUrl(String backgroundImageUrl) {
		this.backgroundImageUrl = backgroundImageUrl;
	}

	public String getThumbnailImageUrl() {
		return thumbnailImageUrl;
	}

	public void setThumbnailImageUrl(String thumbnailImageUrl) {
		this.thumbnailImageUrl = thumbnailImageUrl;
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getMetaKeyword() {
		return metaKeyword;
	}

	public void setMetaKeyword(String metaKeyword) {
		this.metaKeyword = metaKeyword;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		return "PackageListingModel [id=" + id + ", categoryId=" + categoryId + ", title=" + title + ", slug=" + slug
				+ ", subTitle=" + subTitle + ", description=" + description + ", highlight=" + highlight
				+ ", departureTime=" + departureTime + ", backgroundImageUrl=" + backgroundImageUrl
				+ ", thumbnailImageUrl=" + thumbnailImageUrl + ", pdf=" + pdf + ", isActive=" + isActive + ", orderNum="
				+ orderNum + ", metaTitle=" + metaTitle + ", metaDescription=" + metaDescription + ", metaKeyword="
				+ metaKeyword + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy + ", dateCreated="
				+ dateCreated + ", dateModified=" + dateModified + "]";
	}

}
