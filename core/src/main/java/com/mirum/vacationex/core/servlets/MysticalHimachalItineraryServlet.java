package com.mirum.vacationex.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.MysticalHimachalItinerary;
import com.mirum.vacationex.core.models.PackageExclusionModel;
import com.mirum.vacationex.core.models.PackageInclusionModel;
import com.mirum.vacationex.core.models.PackageIteneraryModel;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post",
		"sling.servlet.paths=/vacationex/MysticalHimachalItinerary" })
public class MysticalHimachalItineraryServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Reference
	private MysticalHimachalItinerary mysticalHimachalItinerary;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		String inclusion = request.getParameter("inclusions");
		String itinerary = request.getParameter("itinerary");
		if ("itinerary".equalsIgnoreCase(itinerary)) {
			mysticalHimachalItinerary.populateDataInclusions();

		} else if ("inclusions".equalsIgnoreCase(inclusion)) {
			mysticalHimachalItinerary.populateDataExclusions();

		} else
			mysticalHimachalItinerary.populateDataItinerary();
	}

}
