package com.mirum.vacationex.core.models;

public class PackageInclusionModel {

	private int id;
	private int packageId;
	private String title;
	private String description;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPackage_id() {
		return packageId;
	}
	public void setPackage_id(int package_id) {
		this.packageId = package_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
