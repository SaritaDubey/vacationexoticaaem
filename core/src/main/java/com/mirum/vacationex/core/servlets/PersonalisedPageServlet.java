package com.mirum.vacationex.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.EmailNotification;
import com.mirum.vacationex.core.dao.PersonalisedPage;
import com.mirum.vacationex.core.models.PersonalisedModal;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post", "sling.servlet.paths=/vacationex/personalised" })
public class PersonalisedPageServlet extends SlingAllMethodsServlet {

	private String name;
	private String emailId;
	private String mobile;
	private String city;
	private int otpVerified;
	private String destination;
	private String requirements;
	private String tnc;

	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Reference
	PersonalisedPage personalised;
	@Reference
	private EmailNotification emailNotification;
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		name = request.getParameter("name");
		emailId = request.getParameter("emailId");
		mobile = request.getParameter("mobile");
		city = request.getParameter("city");
		otpVerified = Integer.parseInt(request.getParameter("otpVerified"));
		destination = request.getParameter("destination");
		requirements = request.getParameter("requirements");
		tnc = request.getParameter("tnc");

		PersonalisedModal modal = new PersonalisedModal();
		modal.setName(name);
		modal.setEmailId(emailId);
		modal.setMobile(mobile);
		modal.setCity(city);
		modal.setOtpVerified(otpVerified);
		modal.setDestination(destination);
		modal.setRequirements(requirements);
		modal.setTnc(tnc);

		try {
			personalised.addPersonalisedInfo(modal);
			emailNotification.sendEmail(name, emailId,mobile,city);
		} catch (SQLException e) {
			logger.error("AddInfo", e);
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(request, response);
	}

}
