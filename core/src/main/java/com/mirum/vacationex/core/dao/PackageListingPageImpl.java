package com.mirum.vacationex.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;
import com.mirum.vacationex.core.models.PackageListingModel;

@Component(service = PackageListingPage.class, property = { "service.description=Test DB Service" })
public class PackageListingPageImpl implements PackageListingPage {
	@Reference
	private DataSourcePool dataSourcePool;
	private Connection con;
	private DataSource dataSource;

	@Override
	public List<PackageListingModel> populateData() {
		List<PackageListingModel> models = new ArrayList<>();
		Logger logger = LoggerFactory.getLogger(getClass());

		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "select id,title,sub_title,description,thumbnail_image_url  from tbl_ve_packages;";
				PreparedStatement statement = con.prepareStatement(query);
				ResultSet resultSet = statement.executeQuery();
				PackageListingModel model;
				while (resultSet.next()) {
					model = new PackageListingModel();
					model.setTitle(resultSet.getString("title"));
					model.setSubTitle(resultSet.getString("sub_title"));
					model.setDescription(resultSet.getString("description"));
					model.setThumbnailImageUrl(resultSet.getString("thumbnail_image_url"));
					models.add(model);
				}
				resultSet.close();
				statement.close();
				return models;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {

					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
