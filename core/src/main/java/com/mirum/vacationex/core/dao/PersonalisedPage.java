package com.mirum.vacationex.core.dao;

import java.sql.SQLException;

import com.mirum.vacationex.core.models.PersonalisedModal;

public interface PersonalisedPage {
	boolean addPersonalisedInfo(PersonalisedModal modal) throws SQLException;
}
