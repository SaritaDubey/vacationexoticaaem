package com.mirum.vacationex.core.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.CorporatePage;
import com.mirum.vacationex.core.dao.EmailNotification;
import com.mirum.vacationex.core.models.CorporateModal;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post", "sling.servlet.paths=/vacationex/corporate" })
public class CorporatePageServlet extends SlingAllMethodsServlet {

	private String companyName;
	private String name;
	private String mobile;
	private String email;
	private String city;
	private int optVerified;
	private String occasion;

	@Reference
	private CorporatePage corporate;
	@Reference
	private EmailNotification emailNotification;

	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		companyName = request.getParameter("companyName");
		name = request.getParameter("name");
		mobile = request.getParameter("mobile");
		//optVerified = Integer.parseInt(request.getParameter("verify-otp"));
		email = request.getParameter("email");
		city = request.getParameter("city");
		occasion = request.getParameter("occasion");

		CorporateModal modal = new CorporateModal();
		modal.setCompanyName(companyName);
		modal.setName(name);
		modal.setMobile(mobile);
		modal.setOptVerified(optVerified);
		modal.setEmail(email);
		modal.setCity(city);
		modal.setOccaion(occasion);

		try {
			response.getWriter().println(corporate.addCorporateInfo(modal));
			emailNotification.sendEmail(name, email,mobile,city);
			response.setContentType("text/html");
		} catch (SQLException e) {
			logger.error("AddInfo", e);
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
