package com.mirum.vacationex.core.models;

public class CampaignModal {

	private int ID;
	private String NAME;
	private int MOBILE;
	private String EMAIL;
	private String TNC;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public int getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(int mOBILE) {
		MOBILE = mOBILE;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getTNC() {
		return TNC;
	}
	public void setTNC(String tNC) {
		TNC = tNC;
	}
	public String getCITY() {
		return CITY;
	}
	public void setCITY(String cITY) {
		CITY = cITY;
	}
	public String getLEAD_SOURCE() {
		return LEAD_SOURCE;
	}
	public void setLEAD_SOURCE(String lEAD_SOURCE) {
		LEAD_SOURCE = lEAD_SOURCE;
	}
	public String getLEAD_SOURCE_SLUG() {
		return LEAD_SOURCE_SLUG;
	}
	public void setLEAD_SOURCE_SLUG(String lEAD_SOURCE_SLUG) {
		LEAD_SOURCE_SLUG = lEAD_SOURCE_SLUG;
	}
	public int getOTP_VERIFIED() {
		return OTP_VERIFIED;
	}
	public void setOTP_VERIFIED(int oTP_VERIFIED) {
		OTP_VERIFIED = oTP_VERIFIED;
	}
	public String getIP_ADDRESS() {
		return IP_ADDRESS;
	}
	public void setIP_ADDRESS(String iP_ADDRESS) {
		IP_ADDRESS = iP_ADDRESS;
	}
	public String getUSER_AGENT() {
		return USER_AGENT;
	}
	public void setUSER_AGENT(String uSER_AGENT) {
		USER_AGENT = uSER_AGENT;
	}
	public String getUTM_SOURCE() {
		return UTM_SOURCE;
	}
	public void setUTM_SOURCE(String uTM_SOURCE) {
		UTM_SOURCE = uTM_SOURCE;
	}
	public String getUTM_MEDIUM() {
		return UTM_MEDIUM;
	}
	public void setUTM_MEDIUM(String uTM_MEDIUM) {
		UTM_MEDIUM = uTM_MEDIUM;
	}
	public String getUTM_CAMPAIGN() {
		return UTM_CAMPAIGN;
	}
	public void setUTM_CAMPAIGN(String uTM_CAMPAIGN) {
		UTM_CAMPAIGN = uTM_CAMPAIGN;
	}
	public String getUTM_TERM() {
		return UTM_TERM;
	}
	public void setUTM_TERM(String uTM_TERM) {
		UTM_TERM = uTM_TERM;
	}
	public String getUTM_CONTENT() {
		return UTM_CONTENT;
	}
	public void setUTM_CONTENT(String uTM_CONTENT) {
		UTM_CONTENT = uTM_CONTENT;
	}
	public String getDATE_CREATED() {
		return DATE_CREATED;
	}
	public void setDATE_CREATED(String dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}
	private String CITY;
	private String LEAD_SOURCE;
	private String LEAD_SOURCE_SLUG;
	private int OTP_VERIFIED;
	private String IP_ADDRESS;
	private String USER_AGENT;
	private String UTM_SOURCE;
	private String UTM_MEDIUM;
	private String UTM_CAMPAIGN;
	private String UTM_TERM;
	private String UTM_CONTENT;
	private String DATE_CREATED;
	
}