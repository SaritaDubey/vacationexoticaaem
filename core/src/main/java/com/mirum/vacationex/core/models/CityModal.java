package com.mirum.vacationex.core.models;

public class CityModal {
	
	private String ID;
	private String DESTINATION;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getDESTINATION() {
		return DESTINATION;
	}
	public void setDESTINATION(String dESTINATION) {
		DESTINATION = dESTINATION;
	}

}
