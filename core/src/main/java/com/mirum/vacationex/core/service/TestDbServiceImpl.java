package com.mirum.vacationex.core.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;

@Component(service=TestDbService.class,property={
	"service.description=Test DB Service"
})
public class TestDbServiceImpl implements TestDbService {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private DataSourcePool dataSourcePool;
	private Connection con;
	private DataSource dataSource;
	
	@Override
	public String getData() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer();
		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			buffer.append("dataSource: ").append(dataSource);
			buffer.append("<br/>");
			con = dataSource.getConnection();
			buffer.append("Connection: ").append(con).append("<br/>");
			
			if(Objects.nonNull(con)) {
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from tbl_ve_contact;");
				while(resultSet.next()) {
					buffer.append("cnt: ").append(resultSet.getInt(1)).append(resultSet.getString(2)).append(resultSet.getString(3)).append(resultSet.getString(4)).append(resultSet.getString(5)).append("<br/>");
				}
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}",e);
			buffer.append("DB Exception:").append(e).append("<br/>");
			e.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					con.close();
		
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return buffer.toString();
	}

}
