package com.mirum.vacationex.core.dao;

import com.mirum.vacationex.core.models.ContactModal;

public interface ContactUs {
	
	public boolean addContactInfo(ContactModal contactModal);

}
