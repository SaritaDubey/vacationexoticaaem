package com.mirum.vacationex.core.service;

import java.sql.Connection;

public interface ConDbService {

	Connection getConnection();
}
