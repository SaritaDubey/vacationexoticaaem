package com.mirum.vacationex.core.dao;

import java.util.List;

import com.mirum.vacationex.core.models.PackageExclusionModel;
import com.mirum.vacationex.core.models.PackageInclusionModel;
import com.mirum.vacationex.core.models.PackageIteneraryModel;

public interface MysticalHimachalItinerary {
	public List<PackageIteneraryModel> populateDataItinerary();
	public List<PackageInclusionModel> populateDataInclusions();
	public List<PackageExclusionModel> populateDataExclusions();

}
