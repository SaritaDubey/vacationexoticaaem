package com.mirum.vacationex.core.models;

public class PackageIteneraryModel {
private int id;
private int packageId;
private String title;
private String shortDescription;
private String longDescription;
private String imageUrl;
public String getImageUrl() {
	return imageUrl;
}
public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getPackageId() {
	return packageId;
}
public void setPackageId(int packageId) {
	this.packageId = packageId;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getShortDescription() {
	return shortDescription;
}
public void setShortDescription(String shortDescription) {
	this.shortDescription = shortDescription;
}
public String getLongDescription() {
	return longDescription;
}
public void setLongDescription(String longDescription) {
	this.longDescription = longDescription;
}

}
