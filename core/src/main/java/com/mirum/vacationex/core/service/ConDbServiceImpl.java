package com.mirum.vacationex.core.service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;

@Component(service=ConDbService.class,property={
		"service.description=Con DB Service"
	})
public class ConDbServiceImpl implements ConDbService {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	private DataSourcePool dataSourcePool;
	private Connection connection;
	private DataSource dataSource;
	

	@Override
	public Connection getConnection() {
		try {
			
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			connection = dataSource.getConnection();
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}",e);
			e.printStackTrace();
		}
		return this.connection;
	}

}
