package com.mirum.vacationex.core.dao;

import java.util.List;

import com.mirum.vacationex.core.models.PackageListingModel;

public interface SearchPage {

	public List<PackageListingModel> SearchData(String description);

	
}
