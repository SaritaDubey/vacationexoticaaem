package com.mirum.vacationex.core.dao;

public interface EmailNotification {

	void sendEmail(String userName,String email, String mobile, String city);
}
