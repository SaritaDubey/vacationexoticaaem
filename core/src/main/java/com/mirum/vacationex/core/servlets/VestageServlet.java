//package com.mirum.vacationex.core.servlets;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.sql.SQLException;
//import java.util.Map;
//
//import javax.servlet.Servlet;
//import javax.servlet.ServletException;
//
//import org.apache.sling.api.SlingHttpServletRequest;
//import org.apache.sling.api.SlingHttpServletResponse;
//import org.apache.sling.api.servlets.SlingAllMethodsServlet;
//import org.osgi.service.component.annotations.Component;
//import org.osgi.service.component.annotations.Reference;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.mirum.vacationex.core.IConnectionPool;
//import com.mirum.vacationex.core.models.CityModal;
//import com.mysql.jdbc.Connection;
//
//@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.methods=get",
//"sling.servlet.methods=post", "sling.servlet.paths=/bin/vestagedb" })
//public class VestageServlet extends SlingAllMethodsServlet {
//
//	@Reference
//	IConnectionPool iConnectionPool;
//	
//	@Override
//	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
//			throws ServletException, IOException {
//		Logger logger = LoggerFactory.getLogger(this.getClass());
//		CityModal cityModal = new CityModal();
//		Connection conn = (Connection) iConnectionPool.getConnection();
//		int id;
//		String destination;
//		Map<String, String[]> map = request.getParameterMap();
//		PrintWriter out = response.getWriter();
//
//		CityModal.setId(map.get("id")[0]);
//		CityModal.setDestination(map.get("destination")[0]);
//		iConnectionPool.addData(cityModal);
//
//	}
//}
