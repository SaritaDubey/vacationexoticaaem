package com.mirum.vacationex.core.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.models.ContactModal;
import com.mirum.vacationex.core.service.ConDbService;



@Component(service = ContactUs.class, property = { "service.description=Adding data of Contact page" })
public class ContactUsImpl implements ContactUs {

	Logger logger = LoggerFactory.getLogger(getClass());
	@Reference
	ConDbService conDbService;
	
	@Override
	public boolean addContactInfo(ContactModal contactModal) {
		Connection con = conDbService.getConnection();
		if(Objects.nonNull(con)) {
			String query = "insert into tbl_ve_contact(name,mobile,email,city) values(?,?,?,?);";


			try {
				PreparedStatement statement = con.prepareStatement(query);
				statement.setString(1, contactModal.getName());
				statement.setString(2, contactModal.getMobile());
				statement.setString(3, contactModal.getEmail());
				statement.setString(4, contactModal.getCity());
				boolean insert=statement.executeUpdate()>0;				
				return insert;
			} catch (SQLException e) {
				logger.error("DB Exception: {}", e);
				e.printStackTrace();
			}finally {
				if (con != null) {
					try {
						con.close();

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return false;
	}
}

