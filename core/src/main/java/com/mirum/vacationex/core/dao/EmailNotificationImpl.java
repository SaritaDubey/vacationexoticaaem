package com.mirum.vacationex.core.dao;

import java.util.Properties;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.models.ContactModal;

@Component(service = EmailNotification.class, property = { "service.description=Adding data of Contact page" })
public class EmailNotificationImpl implements EmailNotification {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String HOST = "smtp.gmail.com";
	private static final int PORT = 465;
	private static final boolean SSL_FLAG = true;

	@Override
	public void sendEmail(String userName, String recepientId, String mobile, String city) {

		String username = "vacationexotica68@gmail.com";
		String password = "vacation@123";
		String fromAddress = "vacationexotica68@gmail.com";
		String subject = "Thanks for getting in touch";
		try {
			String to = recepientId;
			String recepientMobile = mobile;
			String recepientCity = city;
			String from = "vacationexotica68@gmail.com";
			Properties properties = System.getProperties();
			properties.setProperty("mail.smtp.host",HOST);
			properties.setProperty("mail.smtp.user", username);
			properties.put("mail.smtp.socketFactory.port", PORT);
			properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.port", PORT);
			properties.put("mail.smtp.socketFactory.fallback", "false");
			properties.put("mail.smtp.starttls.enable", "true");
			Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText("Dear " +userName+",\n\n"+
			"Thank you for your interest in this spectacular tour by Vacations Exotica."+"\nWe will try to reach you at this number "+recepientMobile+" ."+
			"\n\nFor more information"+"\nToll Free\t: 1800-103-9889"+
			"\nWebsite\t\t: www.vacationsexotica.com"+
			"\nEmail id\t: info@vacationsexotica.com");
			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", fromAddress, password);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}

		catch (Exception ex) {
			ex.printStackTrace();
		}

	}



}
