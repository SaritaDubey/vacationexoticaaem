package com.mirum.vacationex.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;
import com.mirum.vacationex.core.models.PackageExclusionModel;
import com.mirum.vacationex.core.models.PackageInclusionModel;
import com.mirum.vacationex.core.models.PackageIteneraryModel;
import com.mirum.vacationex.core.models.PackageListingModel;

@Component(service = MysticalHimachalItinerary.class, property = { "service.description=Test DB Service" })
public class MysticalHimachalItineraryImpl implements MysticalHimachalItinerary {

	@Reference
	private DataSourcePool dataSourcePool;
	private Connection con;
	private DataSource dataSource;

	@Override
	public List<PackageIteneraryModel> populateDataItinerary() {
		List<PackageIteneraryModel> models = new ArrayList<>();
		Logger logger = LoggerFactory.getLogger(getClass());

		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "select title,short_description,long_description,image_url from tbl_ve_itenerary where package_id=1;";
				PreparedStatement statement = con.prepareStatement(query);
				ResultSet resultSet = statement.executeQuery();
				PackageIteneraryModel model;
				while (resultSet.next()) {
					model = new PackageIteneraryModel();
					model.setTitle(resultSet.getString("title"));
					model.setShortDescription(resultSet.getString("short_description"));
					model.setLongDescription(resultSet.getString("long_description"));
					model.setImageUrl(resultSet.getString("image_url"));
					models.add(model);
				}
				resultSet.close();
				statement.close();
				return models;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {

					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
		

	@Override
	public List<PackageInclusionModel> populateDataInclusions() {
		List<PackageInclusionModel> models = new ArrayList<>();
		Logger logger = LoggerFactory.getLogger(getClass());

		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "select title,description from tbl_ve_inclusion where package_id=1;";
				PreparedStatement statement = con.prepareStatement(query);
				ResultSet resultSet = statement.executeQuery();
				PackageInclusionModel model;
				while (resultSet.next()) {
					model = new PackageInclusionModel();
					model.setTitle(resultSet.getString("title"));
					model.setDescription(resultSet.getString("description"));
					
					models.add(model);
				}
				resultSet.close();
				statement.close();
				return models;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {

					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public List<PackageExclusionModel> populateDataExclusions() {
		List<PackageExclusionModel> models = new ArrayList<>();
		Logger logger = LoggerFactory.getLogger(getClass());

		try {
			dataSource = (DataSource) dataSourcePool.getDataSource("vestagedb");
			con = dataSource.getConnection();

			if (Objects.nonNull(con)) {
				String query = "select description from tbl_ve_exclusion where package_id=1;;";
				PreparedStatement statement = con.prepareStatement(query);
				ResultSet resultSet = statement.executeQuery();
				PackageExclusionModel model;
				while (resultSet.next()) {
					model = new PackageExclusionModel();
					model.setDescription(resultSet.getString("description"));
					models.add(model);
				}
				resultSet.close();
				statement.close();
				return models;
			}
		} catch (DataSourceNotFoundException | SQLException e) {
			logger.error("DB Exception: {}", e);
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {

					con.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

}
