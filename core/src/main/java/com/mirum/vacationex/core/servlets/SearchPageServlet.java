package com.mirum.vacationex.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.SearchPage;
import com.mirum.vacationex.core.models.PackageListingModel;
import com.mirum.vacationex.core.models.PersonalisedModal;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post", "sling.servlet.paths=/vacationex/search" })
public class SearchPageServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	@Reference
	SearchPage search;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final long serialVersionUID = 1L;

	private String description;
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		description=request.getParameter("description");
		PackageListingModel modal=new PackageListingModel();
		modal.setDescription(description);
		PrintWriter out = response.getWriter();
		//out.println("called hell0.....");

		logger.info("INSIDE THE TEST_SERVLET>>>!!!!!!");
		search.SearchData(description);
		logger.info("Onkar", search.SearchData(description));

	//	out.println(search.SearchData(description).toString());

	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);

	}

}
