package com.mirum.vacationex.core.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.EmailNotification;
import com.mirum.vacationex.core.dao.EnquiryPage;
import com.mirum.vacationex.core.models.EnquiryModal;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post", "sling.servlet.paths=/vacationex/enquiry" })
public class QuickEnquiryServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private String name;
	private String addDesc;
	private String mobile;
	private String email;
	private String tnc;
	private String destination;
	private String city;
	private int otp_verified;

	@Reference
	EnquiryPage enquiry;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Reference
	private EmailNotification emailNotification;
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		name = request.getParameter("name");
		email = request.getParameter("email");
		mobile = request.getParameter("mobile");
		// otp_verified = Integer.parseInt(request.getParameter("otpVerified"));
		city = request.getParameter("city");
		destination = request.getParameter("destination");
		addDesc = request.getParameter("addDesc");
		tnc = request.getParameter("tnc");

		EnquiryModal modal = new EnquiryModal();
		modal.setName(name);
		modal.setEmail(email);
		modal.setMobile(mobile);
		modal.setOtp_verified(otp_verified);
		modal.setCity(city);
		modal.setDestination(destination);
		modal.setAdd_desc(addDesc);
		modal.setTnc(tnc);
		try {
			enquiry.addEnquiryInfo(modal);
			emailNotification.sendEmail(name, email,mobile,city);
		} catch (SQLException e) {
			logger.error("AddInfo", e);
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
