package com.mirum.vacationex.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirum.vacationex.core.dao.PackageListingPage;

@Component(service = Servlet.class, immediate = true, property = {
		"sling.servlet.methods=get" + "sling.servlet.methods=post", "sling.servlet.paths=/vacationex/packageListing" })
public class PackageListingPageServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	@Reference
	PackageListingPage listing;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);

	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out =response.getWriter();
		
		logger.info("INSIDE THE TEST_SERVLET>>>!!!!!!");
		listing.populateData();
		out.println(listing.populateData().toString());
		
	}

}
