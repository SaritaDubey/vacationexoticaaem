package com.mirum.vacationex.core.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = IConnection.class)
public class ConnectionImpl implements IConnection {

	private Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * private static Map<String, Connection> map = new LinkedHashMap<>();
	 * 
	 * static { Connection con = null; try { Class.forName(JDBC_DRIVER);
	 * 
	 * con = DriverManager.getConnection(DB_URL, USER, PASS); for (int i = 0; i <
	 * MAX_COUNT; i++) { map.put(con.toString(), con); COUNT++; } } catch
	 * (ClassNotFoundException | SQLException e) { e.printStackTrace();
	 * 
	 * } }
	 */

	private Connection connection;

	public Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection("jdbc:mysql://localhost/vestagedb", "root", "root");

		} catch (Exception e) {
			// TODO: handle exception
		}

		return this.connection;
	}

}
